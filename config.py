# -*- coding:UTF-8 -*-
from bs4 import BeautifulSoup
from retrying import retry
import requests
from configparser import ConfigParser

# def _result(result):
#     return result is None
class doconfig(object):
    cfg = ConfigParser()
    def readconfig(self):
        cfg.read('config.ini')
        return ''
    
    def writeconfig(self):
        return ''

class downloader(object):
    
    # def reqget(self,target):
    #     # target = 'http://www.qqddc.com/jxs.do?method=list&pn='+str(pagenum)+'&pp='+str(city)
    #     print('请求数据:',target)
    #     res = requests.get(url = target)
    #     if res.status_code != 200:
    #         print('请求失败:',target,' 重新尝试……')
    #         raise requests.RequestException('my_request_get error!!!!')
    #     html = res.text

    # @retry(stop_max_attempt_number=10, wait_random_min=1000, wait_random_max=2000)
    def doit(self,city,pagenum):
        ret = ''
        target = 'http://www.qqddc.com/jxs.do?method=list&pn='+str(pagenum)+'&pp='+str(city)
        print('请求数据:',target)
        res = requests.get(url = target)
        # if res.status_code != 200:
        #     print('请求失败:',target,' 重新尝试……')
        #     raise requests.RequestException('my_request_get error!!!!')
        html = res.text
        bf = BeautifulSoup(html)
        print('解析成功:',target)
        divs = bf.find_all('div', class_ = 'item-txt') 
        for i in divs:
            spans = i.find_all('span')
            onetext =  i.h1.a.text+'\t'+'品牌：'+ spans[0].a.text + '\t' + spans[1].text + '\t' + '地址：' + spans[2].text.split("：")[1]
            ret = ret + onetext + '\n'
        return ret
        #  print(texts.h1[0])

    def writer(self, path, text):
        write_flag = True
        with open(path, 'a', encoding='utf-8') as f:
            f.write('\n')
            f.writelines(text)
            # f.write('\n\n')

if __name__ == "__main__":
    cfg = ConfigParser()
    cfg.read('ddc-config.conf')
    print(cfg.getint('curr','city'))
    cfg.set('curr','city','2')
    with open('ddc-config.conf', 'w') as configfile:
        cfg.write(configfile)
    cfg.read('ddc-config.conf')
    print(cfg.getint('curr','city'))
    