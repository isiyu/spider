var http = require('http');
var fs = require('fs');
var cheerio = require('cheerio');
var Iconv  = require('iconv').Iconv;
const install = require('superagent-charset');
const request = require('superagent');

function getMovies(url){
  var ids = '';
  superagent = install(request);
  superagent.get(url).charset('gb2312').end(function(err,res) {
  	if(err) console.log(err);
    // <a href="/i/98477.html" class="ulink" title="2017年欧美7.0分科幻片《猩球崛起3：终极之战》BD中英双字">2017年欧美7.0分科幻片《猩球崛起3：终极之战》BD中英双字</a>
          var $ = cheerio.load(res.text, {decodeEntities: false});
          $("a.ulink").each(function(i, e) {
                var movieId = $(e).attr("href").replace(/[^0-9]+/g, '');
                // console.log(movieId);
                ids = movieId;
            });
            return ids;
          // console.log('ok');
  });
}
exports.getMovies = getMovies;
